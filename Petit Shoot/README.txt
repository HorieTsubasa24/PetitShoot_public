*************************************************
*                                               *
*         Petit Shoot (プチ・ショット)          *
*                             T.S soft 2017     *
*                                    Ver. 1.0   *
*                                               *
*************************************************
動作環境
Windows10, 8, 8.1, 7
DirectX

Windows10、Windows7で動作確認。



起動方法
Petit Shoot内の、Shu.exeを起動してください。

操作方法

キーボード：
上下左右キーで移動
spaceキーで決定、弾発射

ジョイパッド：
上下左右キーで移動
0ボタンで弾発射

コンティニュー：
タイトル画面で上下キーを押すと
クリアしたステージをセレクトできます。
(クリアステージ、ハイスコアはsave.binに保存されます。)

また、中ボスのあるステージは中ボスを倒すと、
コンティニュー時に中間ポイントから復帰できます。
(中間ポイントはsave.binにセーブされません。)





製作者：堀江翼
連絡先：Twitter https://twitter.com/TerauchiSingo
		メール terauchi.magic@gmail.com

販売サークル：NEO//SOFT
連絡先：代表：中田大智
		Twitter https://twitter.com/kawatisora
		ホームページ http://www.game-neosoft.com/
		